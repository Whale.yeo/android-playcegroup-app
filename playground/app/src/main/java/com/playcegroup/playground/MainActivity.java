package com.playcegroup.playground;

import android.animation.Animator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

    private WebView mWebView;
    private ProgressBar progressBar;

    private Toast finishToast;
    private long backPressMills = 0;
    private PlaycegroupChromeClient chromeClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressBar = findViewById(R.id.progressBar);

        initializeWebView(new Runnable() {
            @Override
            public void run() {
                Log.d("playground", "WebView has been loaded.");
            }
        });
        initializeFinishToast();
        initializeBottomMenu();
        hideSplash(2000);
    }

    private void initializeBottomMenu() {
        View previousButton = findViewById(R.id.bottom_menu_previous);
        View nextButton = findViewById(R.id.bottom_menu_next);

        previousButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mWebView != null && mWebView.canGoBack()) {
                    goBack();
                }
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mWebView != null && mWebView.canGoForward()) {
                    mWebView.goForward();
                }
            }
        });
    }

    @SuppressLint("NewApi")
    private void initializeWebView(Runnable callback) {
        mWebView = findViewById(R.id.webview);

        PlaycegroupWebViewClient webViewClient = new PlaycegroupWebViewClient(progressBar, callback, this);
        mWebView.setWebViewClient(webViewClient);
        chromeClient = new PlaycegroupChromeClient(progressBar, MainActivity.this);
        webViewClient.setChromeClient(chromeClient);
        mWebView.setWebChromeClient(chromeClient);

        WebSettings settings = mWebView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setLoadWithOverviewMode(true);
        settings.setUseWideViewPort(true);
        settings.setSupportZoom(true);
        settings.setBuiltInZoomControls(false);
        settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        settings.setDomStorageEnabled(true);
        settings.setDatabaseEnabled(true);
        settings.setSupportMultipleWindows(true);

        mWebView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        mWebView.setScrollbarFadingEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            mWebView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else {
            mWebView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
        mWebView.loadUrl(C.Web.ROOT);
    }

    @SuppressLint("NewApi")
    private void hideSplash(int startDelay) {
        final View splashView = findViewById(R.id.splash_layout);
        Animator.AnimatorListener listener = new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                splashView.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        };
        splashView.animate().setStartDelay(startDelay).alpha(0).setDuration(500).setListener(listener).start();
    }

    private void initializeFinishToast() {
        finishToast = Toast.makeText(getApplicationContext(), "'뒤로' 버튼을 한번 더 누르시면 종료됩니다.", Toast.LENGTH_SHORT);
        ViewGroup group = (ViewGroup) finishToast.getView();
        TextView messageTextView = (TextView) group.getChildAt(0);
        messageTextView.setTextSize(14);
        messageTextView.setGravity(Gravity.CENTER);
    }

    private void goBack() {
        WebView popupWebView = chromeClient.getPopupWebView();
        if(popupWebView == null) {
            if (mWebView.canGoBack()) {
                mWebView.goBack();
            } else {
                finishApp();
            }
        } else {
            if (popupWebView.canGoBack()) {
                popupWebView.goBack();
            } else {
                chromeClient.closePopup();
            }
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_BACK :
                    goBack();
                    return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    private void finishApp() {
        long t = System.currentTimeMillis();
        if(t - backPressMills < 3000) {
            finish();
            finishToast.cancel();
        } else {
            backPressMills = t;
            finishToast.show();
        }
    }
}
